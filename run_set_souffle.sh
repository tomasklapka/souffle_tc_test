#!/bin/bash

. settings

echo "Running ${N}x '$1' on set '$2'"
for (( I=1; I<=N; I++ )); do $(${1} -F$2 -D${OUT} $3); done
