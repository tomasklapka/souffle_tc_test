#!/bin/bash

. settings

TC=./$OUT/tc

echo "Measuring transitive closure speed"
echo
echo "Time command: '$TIMECMD'"
echo "N-runs: $N"
echo "TC:"
echo
cat ./tc.dl
echo
echo
echo

for SET in 100 200 400 800 1600 3200 6400
do
    echo "$SET interpreted"
    $TIMECMD $RUN_SOUFFLE $SOUFFLE $SET ./tc.dl
    echo
done

echo -n "cleaning..."
rm ./$OUT/path.csv
echo "done."